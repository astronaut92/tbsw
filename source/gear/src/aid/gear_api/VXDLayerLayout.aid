package hep.gear.api ;

/** Abstract description of layers in a Vertex detector. <br>
 *  This assumes a symmetric layout of ladders, arranged in layers equidistant 
 *  to IP.  <br>
 *  The sensitive area is assumed to be inside the ladders but can be positioned independently.
 * 
 * @author R. Lippe, DESY
 * @version $Id: 
 */
public interface VXDLayerLayout {
    
    /** The total number of layers.
     */
    public int getNLayers() const ;

    /** The number of ladders in the layer layerIndex - layer indexing starts at 0
     *  for the layer closest to IP.
     */
    public int getNLadders(int layerIndex) const ;

    /** Azimuthal angle of the (outward pointing) normal of the first ladder.
     *  Phi0==0 corresponds to the first ladder's normal coinciding (if offset==0) with the x-axis.
     *  The layerIndex starts at 0 for the layer closest to IP.
     */
    public double getPhi0(int layerIndex) const ;
  
   /** The radiation length in the support structure ladders of layer layerIndex - layer indexing starts at 0
     *  for the layer closest to IP.
     */
    public double getLadderRadLength(int layerIndex) const ;

    /** The radiation length in sensitive volumes in layer layerIndex - layer indexing starts at 0
     *  for the layer closest to IP.
     */
    public double getSensitiveRadLength(int layerIndex) const ;

    /** The distance of ladders in layer layerIndex from the IP - layer indexing starts at 0
     *  for the layer closest to IP.
     */
    public double getLadderDistance(int layerIndex) const ;

    /** The thickness in mm of the ladders in layerIndex - layer indexing starting at 0
     *  for the layer closest to IP.
     */
    public double getLadderThickness(int layerIndex) const ;

    /** The offset of the ladder in mm defines the shift of the ladder in the direction of increasing phi
     *  perpendicular to the ladder's normal. For example if the first ladder is at phi0==0 then the offset 
     *  defines an upward shift of the ladder (parallel to the y-axis).  
     *  Layer indexing starts at 0 for the layer closest to IP.
     *  @see getPhi0
     *  @see getSensitiveOffset
     */
    public double getLadderOffset(int layerIndex) const ;

    /** The width of the ladder in layer in mm for ladders in layer layerIndex -
     *  layer indexing starting at 0 from the layer closest to IP.
     */
    public double getLadderWidth(int layerIndex) const ;	
    
    /** The length of the ladder in z direction in mm for ladders in layer layerIndex -
     *  layer indexing starting at 0 from the layer closest to IP.
     */
    public double getLadderLength(int layerIndex) const ;


    /** The distance of sensitive area in ladders in layer layerIndex from the IP.
     */
    public double getSensitiveDistance(int layerIndex) const ;

    /** The thickness in mm of the sensitive area in ladders in layer layerIndex.
     */
    public double getSensitiveThickness(int layerIndex) const ;

    /** Same as getLadderOffset() except for the sensitive part of the ladder.
     * @see getLadderOffset
     */
    public double getSensitiveOffset(int layerIndex) const ;

    /** The width of the sensitive area in ladders in layer layerIndex in mm.
     */
    public double getSensitiveWidth(int layerIndex) const ;	
    
    /** The length of the sensitive area in ladders in z direction in mm for ladders in layer layerIndex.
     */
    public double getSensitiveLength(int layerIndex) const ;  
}
