#include "gear/GEAR.h"

namespace gear{

  const char* GEAR::GEARPARAMETERS = "GearParameters" ;
  const char* GEAR::TPCPARAMETERS = "TPCParameters" ;
  const char* GEAR::CALORIMETERPARAMETERS = "CalorimeterParameters" ;
  const char* GEAR::VXDPARAMETERS = "VXDParameters" ;
  const char* GEAR::SIPLANESPARAMETERS = "SiPlanesParameters" ;
  const char* GEAR::TBSIPARAMETERS = "TBSiParameters" ;

}
